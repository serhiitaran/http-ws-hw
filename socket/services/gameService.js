import { getRoom } from "./roomsService";
import { toDefaultRoom } from "../helpers/roomHelper";
import {
  changeStatus,
  checkIsGameCanStart,
  getTextId,
  updateUserProgress,
  checkIsGameEnd,
  removeDisconnectedUsers,
  addNotFinishedUsers
} from "../helpers/gameHelper";
import { MINIMUM_USERS_FOR_GAME_START, SECONDS_TIMER_BEFORE_START_GAME, SECONDS_FOR_GAME } from "../config";
import { texts } from "../../data";

let finishedUsers = [];

export const changeReadyStatus = (roomName, username, socket, io) => {
  const room = getRoom(roomName);
  const { updatedRoom, changedStatus } = changeStatus(room, username);
  io.in(roomName).emit("UPDATE_USERS", updatedRoom);
  const isGameCanStart = checkIsGameCanStart(updatedRoom, MINIMUM_USERS_FOR_GAME_START);
  if (isGameCanStart) {
    const gameStartRoom = { ...updatedRoom, isGameStart: true };
    const textId = getTextId(texts.length);
    io.in(roomName).emit(
      "GAME_READY",
      SECONDS_TIMER_BEFORE_START_GAME,
      gameStartRoom.roomName,
      textId,
      SECONDS_FOR_GAME
    );
    return { isRoomUpdated: true, room: gameStartRoom };
  } else {
    socket.emit("UPDATE_USER_STATUS", changedStatus);
    return { isRoomUpdated: true, room: updatedRoom };
  }
};

export const endGame = (room, io) => {
  const updatedFinishedUsers = removeDisconnectedUsers(room.users, finishedUsers);
  const winners = addNotFinishedUsers(room.users, updatedFinishedUsers);
  const updatedRoom = toDefaultRoom(room);
  finishedUsers = [];
  io.in(room.roomName).emit("GAME_END", winners, updatedRoom);
  return { isRoomUpdated: true, room: updatedRoom };
};

export const updateGameProgress = (roomName, username, updatedProgress, io) => {
  const room = getRoom(roomName);
  const updatedRoom = updateUserProgress(room, username, updatedProgress);
  io.in(roomName).emit("UPDATE_USERS", updatedRoom);
  if (updatedProgress === 100) {
    finishedUsers.push(username);
  }
  const isGameEnd = checkIsGameEnd(updatedRoom, finishedUsers);
  if (isGameEnd) {
    return endGame(updatedRoom, io);
  }
  return { isRoomUpdated: true, room: updatedRoom };
};
