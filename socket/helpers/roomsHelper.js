import { getItemIndex } from "./utils";

const getRoomIndex = (rooms, roomName) => getItemIndex(rooms, roomName, "roomName");

const getUserIndex = (users, username) => getItemIndex(users, username, "username");

export const getRoomByName = (rooms, roomName) => {
  const roomIndex = getRoomIndex(rooms, roomName);
  return rooms[roomIndex];
};

export const updateRoom = (rooms, updatedRoom) => {
  return rooms.map(room => {
    if (room.roomName === updatedRoom.roomName) {
      return updatedRoom;
    }
    return room;
  });
};

export const deleteRoom = (rooms, { roomName }) => {
  return rooms.filter(room => room.roomName !== roomName);
};

export const refreshRooms = (rooms, updatedRoom) => {
  const isRoomEmpty = !updatedRoom.users.length;
  if (isRoomEmpty) {
    const refreshedRooms = deleteRoom(rooms, updatedRoom);
    return { refreshedRooms, status: "deleted" };
  } else {
    const refreshedRooms = updateRoom(rooms, updatedRoom);
    return { refreshedRooms, status: "updated" };
  }
};

export const checkIsUserWasInRoom = (rooms, username) => {
  const room = rooms.filter(room => {
    const userIndex = getUserIndex(room.users, username);
    return userIndex !== -1;
  });
  if (room.length) {
    return { isUserWasInRoom: true, room: room[0] };
  }
  return { isUserWasInRoom: false, room: null };
};
