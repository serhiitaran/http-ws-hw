export const changeStatus = (room, username) => {
  let changedStatus;
  const users = room.users.map(user => {
    if (user.username === username) {
      changedStatus = !user.isReady;
      const updatedUser = { ...user, isReady: changedStatus };
      return updatedUser;
    }
    return user;
  });
  const updatedRoom = { ...room, users };
  return { updatedRoom, changedStatus };
};

export const checkIsGameCanStart = (room, minUsersForGameStart) => {
  const readyUsers = room.users.filter(user => user.isReady);
  return readyUsers.length >= minUsersForGameStart;
};

export const getTextId = sumOfTexts => {
  const min = 0;
  const max = sumOfTexts - 1;
  let randomTextIndex = min + Math.random() * (max + 1 - min);
  return Math.floor(randomTextIndex);
};

export const updateUserProgress = (room, username, progress) => {
  const users = room.users.map(user => {
    if (user.username === username) {
      const updatedUser = { ...user, progress };
      return updatedUser;
    }
    return user;
  });
  return { ...room, users };
};

export const removeDisconnectedUsers = (roomUsers, finishedUsers) => {
  const roomUsernames = roomUsers.map(user => user.username);
  return finishedUsers.filter(user => roomUsernames.includes(user));
};

export const addNotFinishedUsers = (roomUsers, finishedUsers) => {
  const notFinishedUsers = roomUsers.filter(user => user.progress !== 100);
  notFinishedUsers.sort((a, b) => b.progress - a.progress);
  const notFinishedUsernames = notFinishedUsers.map(user => user.username);
  return [...finishedUsers, ...notFinishedUsernames];
};

export const checkIsGameEnd = room => {
  const finishedUsers = room.users.filter(user => user.progress === 100);
  return finishedUsers.length == room.users.length;
};
