import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
import { texts } from "../data";

const router = Router();

router
  .get("/texts/:id", (req, res) => {
    const id = req.params.id;
    res.status(200);
    res.send(texts[id]);
  })
  .get("/", (req, res) => {
    const page = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  });

export default router;
