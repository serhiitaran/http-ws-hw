import {
  renderPrepareToGameView,
  updateText,
  renderGameView,
  updateGameTimer,
  createWinnerList,
  renderRoomPage
} from "../helpers/viewHelper.mjs";
import { gameStartTimer, getText, updateGameText, updateUserProgress } from "../helpers/gameHelper.mjs";
import { showWinnerModal } from "../helpers/modal/winner.mjs";

let gameRoom;
let timeToGameEnd;
let currentСharIndex = 0;
let gameText;
let gameSocket;
let progress;
let isGameOver = false;

const handleGame = event => {
  const isTextUpdated = updateGameText(gameText, currentСharIndex, event.key);
  if (isTextUpdated) {
    currentСharIndex++;
    progress = updateUserProgress(gameText, currentСharIndex);
    gameSocket.emit("UPDATE_GAME_PROGRESS", { roomName: gameRoom, updatedProgress: progress });
  }
};

const startGame = () => {
  document.addEventListener("keydown", handleGame);
  renderGameView(timeToGameEnd);
  let timerId = setInterval(() => {
    updateGameTimer(timeToGameEnd);
    if (timeToGameEnd == 0 || isGameOver) {
      clearInterval(timerId);
    }
    if (timeToGameEnd == 0) {
      gameSocket.emit("GAME_TIME_ENDED", gameRoom);
    }
    timeToGameEnd--;
  }, 1000);
};

export const prepareToGame = (timeToGameStart, roomName, textId, gameTime, socket) => {
  gameRoom = roomName;
  timeToGameEnd = gameTime;
  gameSocket = socket;
  isGameOver = false;
  currentСharIndex = 0;

  renderPrepareToGameView(timeToGameStart);
  gameStartTimer(timeToGameStart, startGame);
  getText(textId).then(text => {
    updateText(text);
    gameText = text;
  });
};

export const endGame = (winners, updatedRoom) => {
  isGameOver = true;
  document.removeEventListener("keydown", handleGame);

  const winnersList = createWinnerList(winners);
  showWinnerModal(winnersList, () => renderRoomPage(updatedRoom));
};
