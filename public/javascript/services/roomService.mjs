import {
  renderRoom,
  renderRooms,
  renderRoomPage,
  hideElement,
  displayElement,
  renderRoomUsersList,
  updateReadyButton
} from "../helpers/viewHelper.mjs";

let roomName;
export const createRoom = socket => {
  const roomName = prompt("Enter room name");
  if (roomName && roomName.trim().length > 1) {
    socket.emit("CREATE_ROOM", roomName);
  } else {
    alert("The room name should have at least 2 characters.");
  }
};

const joinRoom = (socket, roomName) => {
  socket.emit("JOIN_ROOM", roomName);
};

export const updateRooms = (roomsContainer, rooms, socket) => {
  const roomsElements = rooms.map(({ roomName, users }) => {
    const handleJoinButton = () => joinRoom(socket, roomName);
    return renderRoom(roomName, users.length, handleJoinButton);
  });
  renderRooms(roomsContainer, roomsElements);
};

export const joinedToRoom = (roomsPage, gamePage, room) => {
  roomName = room.roomName;
  hideElement(roomsPage);
  renderRoomPage(room);
  displayElement(gamePage);
};

export const leaveRoom = (socket, gamePage, roomsPage) => {
  socket.emit("LEAVE_ROOM", roomName);
  hideElement(gamePage);
  displayElement(roomsPage);
};

export const updateUserList = room => renderRoomUsersList(room.users);

export const changeReadyStatus = socket => {
  socket.emit("CHANGE_READY_STATUS", roomName);
};

export const updateUserStatus = changedStatus => updateReadyButton(changedStatus);
