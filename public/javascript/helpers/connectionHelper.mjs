export const getUsername = () => sessionStorage.getItem("username");

export const redirectToLogin = () => window.location.replace("/login");

export const handleСonnectionError = error => {
  alert(error);
  sessionStorage.clear();
  redirectToLogin();
};
