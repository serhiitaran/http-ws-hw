import { showModal } from "./modal.mjs";

export function showWinnerModal(winners, renderRoom) {
  const title = "Winners";
  const bodyElement = winners;
  const onClose = () => renderRoom();
  showModal({
    title,
    bodyElement,
    onClose
  });
}
